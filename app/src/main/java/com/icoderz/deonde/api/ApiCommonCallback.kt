package com.icoderz.deonde.api

import com.icoderz.deonde.ui.deliveryAddress.DeliveryAddress

interface ApiCommonCallback<T> {
    fun onSuccess(response: T)

    fun onError(model: DeliveryAddress)

    fun onUnauthenticated()
}