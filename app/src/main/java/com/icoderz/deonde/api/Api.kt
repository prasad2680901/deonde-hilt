package com.icoderz.deonde.api

import com.icoderz.deonde.ui.authenticaton.model.AuthenticationResponse
import com.icoderz.deonde.ui.deliveryAddress.model.AddressResponse
import com.icoderz.deonde.ui.home.model.BannerResponse
import com.icoderz.deonde.ui.home.model.BrandListResponse
import com.icoderz.deonde.ui.home.model.NearByRestaurantResponse
import com.icoderz.deonde.ui.restaurants.model.RestaurantDetailResponseModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface Api {

    @FormUrlEncoded
    @POST("registration")
    fun register(
        @Field("user_email") userEmail: String,
        @Field("mobile_number") mobileNo: String,
        @Field("user_name") userName: String,
        @Field("password") password: String,
        @Field("vendor_id") venderId: Int,
        @Field("device_model") deviceModel: String,
        @Field("devicetype") deviceType: String,
        @Field("time_zone") timeZone: Int,
        @Field("os") os: String,
        @Field("app_version") appVersion: Int,
        @Field("country") country: String,
    ): Call<AuthenticationResponse>

    @Headers("Content-Type:application/json")
    @POST("login")
    fun authentication(
        @Body params: HashMap<String, String>
    ): Call<AuthenticationResponse>

    @FormUrlEncoded
    @POST("list_shiping_address")
    fun getShipingAddress(@FieldMap params: HashMap<String, String>): Call<AddressResponse>

    @FormUrlEncoded
    @POST("get_banner")
    fun getBanners(@FieldMap params: HashMap<String, String>): Call<BannerResponse>

    @FormUrlEncoded
    @POST("brand-list")
    fun getBrandList(@FieldMap params: HashMap<String, String>): Call<BrandListResponse>

    @FormUrlEncoded
    @POST("get_nearby_restaurant")
    fun getNearByRestaurant(@FieldMap params: HashMap<String, String>): Call<NearByRestaurantResponse>

    @FormUrlEncoded
    @POST("restaurantcategorymenus")
    fun getRestaurantCategoryMenu(@FieldMap params: HashMap<String, String>): Call<RestaurantDetailResponseModel>
}