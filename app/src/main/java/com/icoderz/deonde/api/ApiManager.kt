package com.icoderz.deonde.api

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import com.icoderz.deonde.ui.authenticaton.model.AuthenticationResponse
import com.icoderz.deonde.ui.deliveryAddress.model.AddressResponse
import com.icoderz.deonde.ui.home.Home
import com.icoderz.deonde.ui.home.model.BannerResponse
import com.icoderz.deonde.ui.home.model.BrandListResponse
import com.icoderz.deonde.ui.home.model.NearByRestaurantResponse
import com.icoderz.deonde.ui.restaurants.model.RestaurantDetailResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiManager {

    fun getData(
        email: String,
        password: String,
        username: String,
        phoneNo: String,
        callback: (AuthenticationResponse) -> Unit
    ) {

        val retrofit = ApiClient.getClient()?.create(Api::class.java)
            ?.register(email, phoneNo, username, password, 40818, "SM30F", "A", 530, "A", 1, "INR")


        retrofit?.enqueue(object : Callback<AuthenticationResponse> {
            override fun onResponse(
                call: Call<AuthenticationResponse>, response: Response<AuthenticationResponse>
            ) {
                if (response.isSuccessful) {
                    val data: AuthenticationResponse = response.body() as AuthenticationResponse
                    callback(data)
                }
            }

            override fun onFailure(call: Call<AuthenticationResponse>, t: Throwable) {
                Log.d("Retrofit", "Get Data Response Failed")

            }
        })
    }

    fun authentication(
        context: Context,
        email: String,
        password: String,
        deviceToken: String,
        sharedPreferences: SharedPreferences
    ) {
        val retrofit = ApiClient.getClient()?.create(Api::class.java)
        val params = HashMap<String, String>().apply {
            put("password", password)
            put("unique_id", "")
            put("vendor_id", "40818")
            put("devicetoken", deviceToken)
            put("email", email)
            put("devicetype", "A")
            put("is_langauge", "en")
        }

        retrofit?.authentication(params)?.enqueue(object : Callback<AuthenticationResponse> {
            override fun onFailure(call: Call<AuthenticationResponse>, t: Throwable) {
                Log.d("Retrofit", "Authentication Response Failed")
            }

            override fun onResponse(
                call: Call<AuthenticationResponse>, response: Response<AuthenticationResponse>
            ) {
                if (response.isSuccessful && response.body()?.code == 200) {
                    sharedPreferences.edit().putBoolean("logged", true).apply()
                    val intent = Intent(context, Home::class.java)
                    context.startActivity(intent)
                } else {
                    Toast.makeText(
                        context, "Login failed! ${response.body()?.msg}", Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    fun shipingAddressList(callback: (AddressResponse) -> Unit) {
        val retrofit = ApiClient.getClient()?.create(Api::class.java)
        val params = HashMap<String, String>().apply {
            put("user_id", "145961")
        }

        retrofit?.getShipingAddress(params)?.enqueue(object : Callback<AddressResponse> {
            override fun onResponse(
                call: Call<AddressResponse>, response: Response<AddressResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body()?.code == 200) {
                        callback(response.body()!!)
                    }
                }
            }

            override fun onFailure(call: Call<AddressResponse>, t: Throwable) {
                Log.d("Retrofit", "Shipping Address Response Failed")

            }
        })
    }

    fun getBanners(latitude: String?, longitude: String?, callback: (List<String>) -> Unit) {
        val retrofit = ApiClient.getClient()?.create(Api::class.java)
        val params = HashMap<String, String>().apply {
            put("vendor_id", "40818")
            put("latitude", latitude!!)
            put("longitude", longitude!!)

        }
        retrofit?.getBanners(params)?.enqueue(object : Callback<BannerResponse> {
            override fun onResponse(
                call: Call<BannerResponse>, response: Response<BannerResponse>
            ) {
                if (response.isSuccessful && response.body()?.code == 200) {
                    callback(response.body()!!.Result.map { it.image })
                }
            }

            override fun onFailure(call: Call<BannerResponse>, t: Throwable) {
                Log.d("Retrofit", "Banner Response Failed")

            }
        })
    }


    fun getBrandList(
        latitude: String?, longitude: String?, callback: (BrandListResponse) -> Unit
    ) {
        val retrofit = ApiClient.getClient()?.create(Api::class.java)
        val params = HashMap<String, String>().apply {
            put("latitude", latitude!!)
            put("longitude", longitude!!)
            put("vendor_id", "40818")
        }
        retrofit?.getBrandList(params)?.enqueue(object : Callback<BrandListResponse> {
            override fun onResponse(
                call: Call<BrandListResponse>, response: Response<BrandListResponse>
            ) {
                if (response.isSuccessful && response.body()?.code == 200) {
                    callback(response.body()!!)
                }
            }

            override fun onFailure(call: Call<BrandListResponse>, t: Throwable) {
                Log.d("Retrofit", "Brand List Response Failed")

            }
        })
    }

    fun getNearByRestaurant(
        latitude: String?,
        longitude: String?,
        callback: (NearByRestaurantResponse) -> Unit
    ) {
        val retrofit = ApiClient.getClient()?.create(Api::class.java)
        val params = HashMap<String, String>().apply {
            put("vendor_id", "40818")
            put("skip", "0")
            put("latitude", latitude ?: "23")
            put("longitude", longitude ?: "72")
        }
        retrofit?.getNearByRestaurant(params)?.enqueue(object : Callback<NearByRestaurantResponse> {
            override fun onResponse(
                call: Call<NearByRestaurantResponse>, response: Response<NearByRestaurantResponse>
            ) {
                if (response.isSuccessful && response.body()?.code == 200) {
                    callback(response.body()!!)
                }
            }

            override fun onFailure(call: Call<NearByRestaurantResponse>, t: Throwable) {
                Log.d("Retrofit", "Near By Restaurant Response Failed")


            }
        })
    }


    fun getRestaurantCategoryMenu(
        restaurantId: String?,
        callback: (RestaurantDetailResponseModel) -> Unit
    ) {
        val retrofit = ApiClient.getClient()?.create(Api::class.java)
        val params = HashMap<String, String>().apply {
            put("restaurant_id", restaurantId.toString())
            put("vendor_id", "40818")
            put("is_langauge", "en")

        }
        retrofit?.getRestaurantCategoryMenu(params)
            ?.enqueue(object : Callback<RestaurantDetailResponseModel> {
                override fun onResponse(
                    call: Call<RestaurantDetailResponseModel>,
                    response: Response<RestaurantDetailResponseModel>
                ) {
                    if (response.isSuccessful && response.body()?.code == "200") {
                        response.body()!!.msg?.let { Log.d("Response", it) }
                        callback(response.body()!!)
                    }
                }

                override fun onFailure(call: Call<RestaurantDetailResponseModel>, t: Throwable) {
                    Log.d("Retrofit ", "Restaurant Category Menu Failed!!")
                }
            })
    }
}