package com.icoderz.deonde.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.icoderz.deonde.R
import com.icoderz.deonde.ui.home.model.ResultData

class RestaurantAdapter(
  private var restaurants : List<ResultData>?,
    private var onClickListner: OnClickListner
) : RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder>() {

    inner class RestaurantViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        private val restaurantNameTV : TextView = itemView.findViewById(R.id.restaurantNameTV)
        private val restaurantIV : ImageView = itemView.findViewById(R.id.restaurantsIV)
        private val restaurantSlugTV : TextView = itemView.findViewById(R.id.restaurantTypeTV)
        private val deliveryTimeTV : TextView = itemView.findViewById(R.id.deliveryTimeTV)
        private val ratingsTV : TextView = itemView.findViewById(R.id.ratingsTV)

        fun bind(restaurant: ResultData) {

            Glide.with(restaurantIV).load(restaurant.icon_image).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(restaurantIV)

            restaurantNameTV.text = restaurant.name
            restaurantSlugTV.text = restaurant.slug
            deliveryTimeTV.text = restaurant.delivery_time
            ratingsTV.text = restaurant.rating.toString()
            itemView.findViewById<CardView>(R.id.restaurantCV).setOnClickListener {
                onClickListner.onClick(restaurant.restaurant_id)
            }
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RestaurantAdapter.RestaurantViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.restaurant_container,parent,false)
        return RestaurantViewHolder(view)
    }

    override fun onBindViewHolder(holder: RestaurantAdapter.RestaurantViewHolder, position: Int) {
            holder.bind(restaurants?.get(position)!!)
    }

    override fun getItemCount(): Int = restaurants?.size!!

    interface OnClickListner{
        fun onClick(restaurantId : Int?)
    }

}