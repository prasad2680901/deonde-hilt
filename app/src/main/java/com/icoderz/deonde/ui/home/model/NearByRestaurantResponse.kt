package com.icoderz.deonde.ui.home.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NearByRestaurantResponse(
    val Result: List<ResultData>? = arrayListOf(),
    val code: Int,
    val msg: String,
    val open_restaurant: Int
) : Parcelable


@Parcelize
data class ResultData(
    val app_detail_layout: String? = "",
    val cuisine_name: List<String>? = arrayListOf(),
    val delivery_time: String? = "",
    val delivery_times: String? = "",
    val discount_text: String? = "",
    val distance: Double? = 0.0,
    val franchisee_id: Int? = 0,
    val icon_image: String? = "",
    val item_counts: Int? = 0,
    val menu_category_count: Int? = 0,
    val name: String? = "",
    val rating: Double? = 0.0,
    val restaurant_convince_charge: Double? = 0.0,
    val restaurant_convince_type: String? = "",
    val restaurant_id: Int? = 0,
    val restaurant_on_off: String? = "",
    val restaurant_packaging_charge: String? = "",
    val restaurant_packaging_type: String? = "",
    val restaurant_service_tax: Int? = 0,
    val restaurant_tags: List<RestaurantTag>? = arrayListOf(),
    val review_count: Int? = 0,
    val slug: String? = "",
    val two_person_price: String? = ""
) : Parcelable


@Parcelize
data class RestaurantTag(
    val background_color: String? = "",
    val id: Int? = 0,
    val name: String? = "",
    val secondary_name: String? = "",
    val text_color: String? = ""
) : Parcelable