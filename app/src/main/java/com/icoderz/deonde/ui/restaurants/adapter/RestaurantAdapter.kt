package com.icoderz.deonde.ui.restaurants.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.icoderz.deonde.databinding.CategoryBinding
import com.icoderz.deonde.ui.restaurants.model.RestaurantDetailResponseModel

class RestaurantAdapter(private var restaurantCategoryMenuResponse: RestaurantDetailResponseModel) :
    RecyclerView.Adapter<RestaurantAdapter.RestaurantAdapterViewHolder>() {

    inner class RestaurantAdapterViewHolder(val binding: CategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(restaurantCategoryMenuResult: RestaurantDetailResponseModel.ResultList) {
            binding.itemCategoryTV.text = restaurantCategoryMenuResult.menu_name
            binding.itemMenuRV.adapter = RestaurantItemMenuAdapter(
                restaurantCategoryMenuResult.menu_item_detail
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantAdapterViewHolder {
        return RestaurantAdapterViewHolder(
            CategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RestaurantAdapterViewHolder, position: Int) {
        holder.bind(restaurantCategoryMenuResponse.Result[position])
    }

    override fun getItemCount(): Int {
        return restaurantCategoryMenuResponse.Result.size
    }
}