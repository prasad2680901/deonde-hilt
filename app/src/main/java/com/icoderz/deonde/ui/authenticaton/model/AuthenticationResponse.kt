package com.icoderz.deonde.ui.authenticaton.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class AuthenticationResponse(
    @SerializedName("code") var code: Int? = null,
    @SerializedName("msg") var msg: String? = null,
    @SerializedName("new_user_id") var newUserId: Int? = null,
    @SerializedName("user_details") var userDetails: UserDetails? = UserDetails()
) : Parcelable {
    @Parcelize
    data class UserDetails(
        @SerializedName("user_id") var userId: Int = 0,
        @SerializedName("referral_code") var referralCode: String = "",
        @SerializedName("user_role") var userRole: String = "",
        @SerializedName("mobile_number") var mobileNumber: String = "",
        @SerializedName("country_code") var countryCode: String = "",
        @SerializedName("user_name") var userName: String = "",
        @SerializedName("last_name") var lastName: String = "",
        @SerializedName("user_email") var userEmail: String = "",
        @SerializedName("is_language") var isLanguage: String = "",
        @SerializedName("user_profile") var userProfile: String = "",
        @SerializedName("verify_alcohol_image") var verifyAlcoholImage: String = "",
        @SerializedName("alcohol_status") var alcoholStatus: String = "",
        @SerializedName("user_notification_sound") var userNotificationSound: String = "",
        @SerializedName("status") var status: String = "",
        @SerializedName("is_cod_enable") var isCodEnable: Int = 0,
        @SerializedName("wallet_status") var walletStatus: String = "",
        @SerializedName("is_test_user") var isTestUser: Int = 0,
        @SerializedName("devicetoken") var devicetoken: String = "",
        @SerializedName("wallet_amount") var walletAmount: Int = 0,
        @SerializedName("customise") var customise: String = "",
        @SerializedName("wallet_recharge_limit") var walletRechargeLimit: String = "",
        @SerializedName("maximum_wallet_amount") var maximumWalletAmount: String = "",
        @SerializedName("token") var token: String = "",
        @SerializedName("wallet") var wallet: String = ""
    ) : Parcelable
}