package com.icoderz.deonde.ui.restaurants.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RestaurantDetailResponseModel(
    val Bravges: BravgesObj? = null,
    val Result: List<ResultList> = arrayListOf(),
    val code: String? = "",
    val msg: String? = "",
    val restaurant_details: RestaurantDetails? = null,
    val restaurant_on_off: String? = ""
) : Parcelable {
    @Parcelize
    class BravgesObj : Parcelable

    @Parcelize
    data class ResultList(
        val color_code: String? = "",
        val image: String? = "",
        val is_display_image: String? = "",
        val item_counts: String? = "",
        val menu_description: String? = "",
        val menu_image: String? = "",
        val menu_item_detail: List<MenuItemDetail> = arrayListOf(),
        val menu_name: String? = "",
        val name: String? = "",
        val restaurant_id: String? = "",
        val restaurant_menu_id: String? = ""
    ) : Parcelable {

        @Parcelize
        data class MenuItemDetail(
            val custom_details: List<CustomDetails> = arrayListOf(),
            val image: String? = "",
            val is_alcoholic: String? = "",
            val is_customization: String? = "",
            val is_featured: String? = "",
            val is_sold_out: String? = "",
            val item_description: String? = "",
            val item_id: String? = "",
            val item_images: List<ItemImage> = arrayListOf(),
            val item_name: String? = "",
            val item_packaging_charge: String? = "",
            val item_tags: List<ItemTags> = arrayListOf(),
            val item_tax: String? = "",
            val item_type: String? = "",
            val mrp: String? = "",
            val price: String? = "",
            val quantity: String? = "",
            val restaurant_id: String? = "",
            val time_slot: String? = ""
        ) : Parcelable {
            @Parcelize
            data class ItemImage(
                val image_name: String? = ""
            ) : Parcelable

            @Parcelize
            class CustomDetails : Parcelable

            @Parcelize
            class ItemTags : Parcelable
        }
    }

    @Parcelize
    data class RestaurantDetails(
        val Address: String? = "",
        val CGST: String? = "",
        val SGST: String? = "",
        val address_thai: String? = "",
        val app_detail_layout: String? = "",
        val avg_rating: String? = "",
        val business_licence_number: String? = "",
        val cuisine_name: List<String> = arrayListOf(),
        val delivery_charge: String? = "",
        val delivery_charge_old: String? = "",
        val delivery_time: String? = "",
        val delivery_type_time_slots: String? = "",
        val discount_text: String? = "",
        val email: String? = "",
        val fssai_licence: String? = "",
        val full_address: String? = "",
        val icon_image: String? = "",
        val is_same_day_delivery: String? = "",
        val item_counts: String? = "",
        val latitude: String? = "",
        val location_address: String? = "",
        val longitude: String? = "",
        val menu_category_count: String? = "",
        val minimum_order_value: String? = "",
        val name: String? = "",
        val name_thai: String? = "",
        val payment_option: String? = "",
        val rating: String? = "",
        val restaurant_address: String? = "",
        val restaurant_convince_charge: String? = "",
        val restaurant_convince_type: String? = "",
        val restaurant_id: String? = "",
        val restaurant_item_layout: String? = "",
        val restaurant_on_off: String? = "",
        val restaurant_packaging_charge: String? = "",
        val restaurant_packaging_type: String? = "",
        val restaurant_service_tax: String? = "",
        val restaurant_tags: List<RestaurantTag> = arrayListOf(),
        val restaurant_tax: String? = "",
        val review_count: String? = "",
        val service_fee: String? = "",
        val slug: String? = "",
        val time_slots: String? = "",
        val two_person_price: String? = ""
    ) : Parcelable {
        @Parcelize
        data class RestaurantTag(
            val background_color: String,
            val id: Int,
            val name: String,
            val secondary_name: String,
            val text_color: String
        ) : Parcelable
    }
}