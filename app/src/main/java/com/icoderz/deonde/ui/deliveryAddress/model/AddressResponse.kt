package com.icoderz.deonde.ui.deliveryAddress.model

import android.os.Parcelable
import com.icoderz.deonde.database.entity.ShippingAddressEntity
import kotlinx.parcelize.Parcelize

@Parcelize
data class AddressResponse(
    val Result: List<ResultData>,
    val code: Int,
    val msg: String
) : Parcelable {
    @Parcelize
    data class ResultData(
        val Shiping_address: String,
        val adress_clarification: String,
        val city: String,
        val flat_no: String,
        val house_name: String,
        val is_primary_address: String,
        val landmark: String,
        val latitude: String,
        val longitude: String,
        val shiping_id: Int,
        val state: String,
        val zip: String
    ) : Parcelable {

        fun toEntity() = ShippingAddressEntity(
            shippingId = shiping_id,
            shippingAddress = Shiping_address,
            addressClarification = adress_clarification,
            city = city,
            flatNo = flat_no,
            houseName = house_name,
            isPrimaryAddress = is_primary_address,
            landmark = landmark,
            latitude = latitude,
            logitude = longitude,
            state = state,
            zip = zip
        )

    }
}