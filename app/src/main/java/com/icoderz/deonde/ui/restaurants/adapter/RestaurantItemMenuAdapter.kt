package com.icoderz.deonde.ui.restaurants.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.icoderz.deonde.databinding.RestaurantItemContainerBinding
import com.icoderz.deonde.ui.restaurants.model.RestaurantDetailResponseModel

class RestaurantItemMenuAdapter(private var menuItems: List<RestaurantDetailResponseModel.ResultList.MenuItemDetail>) :
    RecyclerView.Adapter<RestaurantItemMenuAdapter.RestaurantItemViewHolder>() {


    inner class RestaurantItemViewHolder(private val binding: RestaurantItemContainerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            restaurantCategoryMenuItemDetail: RestaurantDetailResponseModel.ResultList.MenuItemDetail
        ) {

            binding.restaurantItemTV.text = restaurantCategoryMenuItemDetail.item_name
            binding.itemDesciptionTV.text = restaurantCategoryMenuItemDetail.item_description
            Glide.with(binding.restaurantsIV).load(restaurantCategoryMenuItemDetail.image)
                .diskCacheStrategy(
                    DiskCacheStrategy.NONE
                ).skipMemoryCache(true).into(binding.restaurantsIV)
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RestaurantItemMenuAdapter.RestaurantItemViewHolder {
        return RestaurantItemViewHolder(
            RestaurantItemContainerBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(
        holder: RestaurantItemMenuAdapter.RestaurantItemViewHolder,
        position: Int
    ) {
        holder.bind(menuItems[position])
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }
}
