package com.icoderz.deonde.ui.deliveryAddress

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.icoderz.deonde.api.ApiManager
import com.icoderz.deonde.database.AddressViewModel
import com.icoderz.deonde.database.UserViewModel
import com.icoderz.deonde.databinding.ActivityDeliveryAddressScreenBinding
import com.icoderz.deonde.ui.deliveryAddress.adapter.AddressAdapter

class DeliveryAddress : AppCompatActivity() {

    private lateinit var binding: ActivityDeliveryAddressScreenBinding
    private lateinit var addressAdapter: AddressAdapter
    private lateinit var mViewModel: UserViewModel
    private lateinit var mAddressViewModel: AddressViewModel
    private lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.statusBarColor = resources.getColor(android.R.color.darker_gray)
        binding = ActivityDeliveryAddressScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        sharedPreferences = getSharedPreferences("selectedAddress", MODE_PRIVATE)

        mViewModel =
            UserViewModel.UserViewModelFactory(application).create(UserViewModel::class.java)

        mAddressViewModel = AddressViewModel.AddressViewModelFactory(application)
            .create(AddressViewModel::class.java)

        binding.toolbar.setNavigationOnClickListener { onBackPressedDispatcher.onBackPressed() }
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.progressBar.visibility = View.VISIBLE

        ApiManager().shipingAddressList { addressResponse ->
            addressAdapter =
                AddressAdapter(addressResponse.Result, object : AddressAdapter.OnClickListener {

                    override fun onClick(position: Int, address: String) {

                        sharedPreferences.edit()
                            .putString("address", addressResponse.Result[position].Shiping_address)
                            .apply()

                        sharedPreferences.edit()
                            .putString("latitude", addressResponse.Result[position].latitude)
                            .apply()

                        sharedPreferences.edit()
                            .putString("longitude", addressResponse.Result[position].longitude)
                            .apply()

                        val shippingAddress = sharedPreferences.getString("address", "")
                        val latitude = sharedPreferences.getString("latitude", "")
                        val longitude = sharedPreferences.getString("longitude", "")
                        val intent = Intent()

                        intent.putExtra("address", shippingAddress)
                        intent.putExtra("latitude", latitude)
                        intent.putExtra("longitude", longitude)
                        setResult(RESULT_OK, intent)
                        finish()
                    }
                })

            addressResponse.Result.map {
                mAddressViewModel.addAddress(it.toEntity())
            }

            binding.recyclerView.adapter = addressAdapter
            binding.progressBar.visibility = View.GONE

        }
    }
}