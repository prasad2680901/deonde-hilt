package com.icoderz.deonde.ui.deliveryAddress.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.icoderz.deonde.databinding.ItemRowBinding
import com.icoderz.deonde.ui.deliveryAddress.model.AddressResponse

class AddressAdapter(
    private var shipingList: List<AddressResponse.ResultData>,
    var onClickListner: OnClickListener
) :
    RecyclerView.Adapter<AddressAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setData(position: Int, resultData: AddressResponse.ResultData) {
            binding.placeTextView.text = resultData.adress_clarification
            binding.addressTextView.text = resultData.Shiping_address
            binding.root.setOnClickListener {
                onClickListner.onClick(position, resultData.Shiping_address)
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressAdapter.ViewHolder {
        return ViewHolder(ItemRowBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: AddressAdapter.ViewHolder, position: Int) {
        holder.setData(position, shipingList[position])
    }

    override fun getItemCount(): Int {
        return shipingList.size
    }

    interface OnClickListener {
        fun onClick(position: Int, address: String)
    }

}