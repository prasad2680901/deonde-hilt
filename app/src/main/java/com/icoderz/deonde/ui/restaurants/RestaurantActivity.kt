package com.icoderz.deonde.ui.restaurants

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.icoderz.deonde.api.ApiManager
import com.icoderz.deonde.databinding.ActivityRestaurantBinding
import com.icoderz.deonde.ui.restaurants.adapter.RestaurantAdapter

class RestaurantActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRestaurantBinding
    private lateinit var restaurantId: String
    private lateinit var restaurantAdapter: RestaurantAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRestaurantBinding.inflate(layoutInflater)
        setContentView(binding.root)

        restaurantId = intent.getIntExtra("restaurantId", 0).toString()
        binding.restaurantPB.visibility = View.VISIBLE

        ApiManager().getRestaurantCategoryMenu(restaurantId) { restaurantCategoryMenuResponse ->

            restaurantCategoryMenuResponse.Result.map {
                binding.menuCategoryTL.addTab(binding.menuCategoryTL.newTab().setText(it.menu_name))
            }

            binding.itemsRV.adapter = RestaurantAdapter(restaurantCategoryMenuResponse)

            restaurantAdapter = RestaurantAdapter(restaurantCategoryMenuResponse)

            binding.menuCategoryTL.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {

                override fun onTabSelected(tab: TabLayout.Tab?) {

                    val position =
                        restaurantCategoryMenuResponse.Result.indexOfFirst { it.menu_name == tab?.text }
                    (binding.itemsRV.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
                        position,
                        0
                    )
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                }

                override fun onTabReselected(tab: TabLayout.Tab?) {
                }
            })

            binding.itemsRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val layoutManager = binding.itemsRV.layoutManager as LinearLayoutManager
                    val firstCompletelyVisibleItemIndex =
                        layoutManager.findFirstVisibleItemPosition()

                    binding.menuCategoryTL.getTabAt(firstCompletelyVisibleItemIndex)?.select()
                }
            }
            )
            binding.restaurantPB.visibility = View.GONE
        }
    }
}