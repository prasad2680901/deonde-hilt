package com.icoderz.deonde.ui.home.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BannerResponse(
    val Result: List<BannerResult>,
    val code: Int,
    val msg: String
) : Parcelable

@Parcelize
data class BannerResult(
    val banner_restaurant: List<BannerRestaurant>,
    val id: Int,
    val image: String,
    val link: String,
    val menu_category_id: Int,
    val name: String,
    val status: String,
    val type: String
) : Parcelable

@Parcelize
data class BannerRestaurant(
    val banner_id: Int,
    val created_at: String,
    val id: Int,
    val restaurant_id: Int,
    val slug: String,
    val updated_at: String
) : Parcelable