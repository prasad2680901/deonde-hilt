package com.icoderz.deonde.ui.home.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BrandListResponse(
    val Result: List<Result>? = arrayListOf(),
    val code: Int,
    val msg: String
) : Parcelable

@Parcelize
data class Result(
    val id: Int? = 0,
    val image: String? = "",
    val logo: String? = "",
    val name: String? = "",
    val restaurants: List<String>? = arrayListOf(),
    val restaurants_slug: List<String>? = arrayListOf(),
    val secondary_name: String? = "",
    val status: String? = "",
    val vendor_id: Int? = 0
) : Parcelable