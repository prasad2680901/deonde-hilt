package com.icoderz.deonde.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.icoderz.deonde.R
import com.icoderz.deonde.ui.home.model.Result

class BrandsAdapter(private val brands: List<Result>?) :
    RecyclerView.Adapter<BrandsAdapter.BrandsViewHolder>() {


    inner class BrandsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageView: ImageView = itemView.findViewById(R.id.brandLogo)
        private val textView: TextView = itemView.findViewById(R.id.brandNameTV)

        fun bind(brand: Result) {
            Glide.with(imageView).load(brand.image).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imageView)
            textView.text = brand.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandsViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.brands_container, parent, false)
        return BrandsViewHolder(view)
    }

    override fun getItemCount(): Int = brands?.size!!

    override fun onBindViewHolder(holder: BrandsViewHolder, position: Int) {
        holder.bind(brands?.get(position)!!)
    }

}