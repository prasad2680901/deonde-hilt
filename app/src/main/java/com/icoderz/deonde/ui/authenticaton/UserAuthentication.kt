package com.icoderz.deonde.ui.authenticaton

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.icoderz.deonde.R
import com.icoderz.deonde.api.ApiManager
import com.icoderz.deonde.database.UserViewModel
import com.icoderz.deonde.database.entity.UserEntity
import com.icoderz.deonde.databinding.ActivityUserAuthenticationScreenBinding
import com.icoderz.deonde.ui.home.Home

class UserAuthentication : AppCompatActivity() {
    private lateinit var binding: ActivityUserAuthenticationScreenBinding
    private lateinit var mViewModel: UserViewModel
    private lateinit var sharedPreference: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val window = window
        window.statusBarColor = resources.getColor(R.color.orange)

        sharedPreference = getSharedPreferences("login", MODE_PRIVATE)
        if (sharedPreference.getBoolean("logged", false)) {
            val intent = Intent(this@UserAuthentication, Home::class.java)
            startActivity(intent)
        }

        mViewModel =
            UserViewModel.UserViewModelFactory(application).create(UserViewModel::class.java)


        binding = ActivityUserAuthenticationScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loginBtn.setOnClickListener {
            ApiManager().authentication(
                this,
                binding.emailET.text.toString(),
                binding.passwordET.text.toString(),
                "",
                sharedPreference
            )
        }

        binding.signUpBtn.setOnClickListener {
            signUpVisibility()
        }
        binding.registerBtn.setOnClickListener {

            ApiManager().getData(
                binding.emailET.text.toString(),
                binding.passwordET.text.toString(),
                binding.userNameET.text.toString(),
                binding.phoneNoET.text.toString()
            ) { data ->

                if (data.code == 200) {
                    data.userDetails?.let {
                        insertApiResponse(
                            it.userId,
                            it.referralCode,
                            it.userRole,
                            it.mobileNumber,
                            it.countryCode,
                            it.userName,
                            it.lastName,
                            it.userEmail,
                            it.isLanguage,
                            it.userProfile,
                            it.verifyAlcoholImage,
                            it.alcoholStatus,
                            it.userNotificationSound,
                            it.status,
                            it.isCodEnable,
                            it.walletStatus,
                            it.isTestUser,
                            it.devicetoken,
                            it.walletAmount,
                            it.customise,
                            it.walletRechargeLimit,
                            it.maximumWalletAmount,
                            it.token,
                            it.wallet,
                        )
                    }
                    Toast.makeText(this, data.msg, Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, data.msg, Toast.LENGTH_LONG).show()

                }
            }
            loginVisibility()

        }
    }

    private fun insertApiResponse(
        userId: Int,
        referralCode: String,
        userRole: String,
        mobileNumber: String,
        countryCode: String?,
        userName: String,
        lastName: String?,
        userEmail: String,
        language: String,
        userProfile: String,
        verifyAlcoholImage: String,
        alcoholStatus: String,
        userNotificationSound: String?,
        status: String,
        codEnable: Int,
        walletStatus: String,
        testUser: Int,
        devicetoken: String,
        walletAmount: Int,
        customise: String?,
        walletRechargeLimit: String?,
        maximumWalletAmount: String?,
        token: String,
        wallet: String?
    ) {
        val user = UserEntity(
            userId,
            referralCode,
            userRole,
            mobileNumber,
            countryCode,
            userName,
            lastName,
            userEmail,
            language,
            userProfile,
            verifyAlcoholImage,
            alcoholStatus,
            userNotificationSound,
            status,
            codEnable,
            walletStatus,
            testUser,
            devicetoken,
            walletAmount,
            customise,
            walletRechargeLimit,
            maximumWalletAmount,
            token,
            wallet
        )
        mViewModel.addUser(user)
    }

    @SuppressLint("SetTextI18n")
    private fun signUpVisibility() {

        binding.orTV.visibility = View.GONE
        binding.signUpBtn.visibility = View.GONE
        binding.loginBtn.visibility = View.GONE

        binding.userNameET.visibility = View.VISIBLE
        binding.phoneNoET.visibility = View.VISIBLE
        binding.registerBtn.visibility = View.VISIBLE

        binding.titleTV.text = "SIGN UP"
    }

    @SuppressLint("SetTextI18n")
    private fun loginVisibility() {

        binding.orTV.visibility = View.VISIBLE
        binding.signUpBtn.visibility = View.VISIBLE
        binding.loginBtn.visibility = View.VISIBLE

        binding.userNameET.visibility = View.GONE
        binding.phoneNoET.visibility = View.GONE
        binding.registerBtn.visibility = View.GONE

        binding.titleTV.text = "LOGIN"
    }

}