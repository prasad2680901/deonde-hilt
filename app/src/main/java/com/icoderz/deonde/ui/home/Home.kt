package com.icoderz.deonde.ui.home

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.size
import com.icoderz.deonde.R
import com.icoderz.deonde.api.ApiManager
import com.icoderz.deonde.databinding.ActivityHomeScreenBinding
import com.icoderz.deonde.ui.deliveryAddress.DeliveryAddress
import com.icoderz.deonde.ui.home.adapter.BrandsAdapter
import com.icoderz.deonde.ui.home.adapter.RestaurantAdapter
import com.icoderz.deonde.ui.home.adapter.SliderAdapter
import com.icoderz.deonde.ui.restaurants.RestaurantActivity


class Home : AppCompatActivity() {

    private lateinit var binding: ActivityHomeScreenBinding
    private var latitude: String? = null
    private var longitude: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val window = window
        window.statusBarColor = resources.getColor(R.color.orange)
        binding = ActivityHomeScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setVisibility(binding)

        fun startAutoSlide() {

            val handler = Handler(Looper.myLooper()!!)
            lateinit var runnable: Runnable

            runnable = Runnable {
                Log.d("TAG", "startAutoSlide: ${binding.viewPager.size}")

                val nextPosition =
                    if (binding.viewPager.size == binding.viewPager.currentItem) {
                        0
                    } else {
                        binding.viewPager.currentItem + 1
                    }

                binding.viewPager.setCurrentItem(nextPosition, true)
                handler.postDelayed(runnable, 3000)
            }

            handler.postDelayed(runnable, 3000)
        }

        val activityLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
                if (activityResult.resultCode == RESULT_OK) {

                    latitude = activityResult.data?.getStringExtra("latitude")
                    longitude = activityResult.data?.getStringExtra("longitude")
                    binding.tvAddress.text = activityResult.data?.getStringExtra("address")

                    ApiManager().getBanners(latitude, longitude) { bannerResponse ->

                        binding.viewPager.adapter = SliderAdapter(bannerResponse)
                        startAutoSlide()
                    }

                    ApiManager().getBrandList(latitude, longitude) { brandListResponse ->
                        binding.brandsRV.adapter = BrandsAdapter(brandListResponse.Result)
                    }
                }

                ApiManager().getNearByRestaurant(
                    latitude,
                    longitude
                ) { nearByRestaurantResponse ->

                    binding.restaurantsRV.adapter =
                        RestaurantAdapter(nearByRestaurantResponse.Result,
                            object : RestaurantAdapter.OnClickListner {
                                override fun onClick(restaurantId: Int?) {
                                    val intent = Intent(this@Home, RestaurantActivity::class.java)
                                    intent.putExtra("restaurantId", restaurantId)
                                    startActivity(intent)

                                }
                            })
                }
                resetVisibility(binding)
            }
        binding.cardViewAddress.setOnClickListener {
            val intent = Intent(this, DeliveryAddress::class.java)
            activityLauncher.launch(intent)
        }
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                finishAffinity()
            }
        }

        onBackPressedDispatcher.addCallback(this, callback)

    }

    private fun setVisibility(binding: ActivityHomeScreenBinding) {

        binding.homeTV.visibility = View.VISIBLE
        binding.nestedScroll.visibility = View.GONE
    }

    private fun resetVisibility(binding: ActivityHomeScreenBinding) {

        binding.homeTV.visibility = View.GONE
        binding.nestedScroll.visibility = View.VISIBLE
    }

}


