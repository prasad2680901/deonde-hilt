package com.icoderz.deonde.ui.splash



import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.icoderz.deonde.databinding.ActivitySplashScreenBinding
import com.icoderz.deonde.ui.authenticaton.UserAuthentication

@SuppressLint("CustomSplashScreen")
class SplashScreen : AppCompatActivity() {
    private lateinit var binding : ActivitySplashScreenBinding

    @SuppressLint("SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler(Looper.getMainLooper()).postDelayed({
        val intent = Intent(this, UserAuthentication::class.java)
            startActivity(intent)
            finish()
        },3000)

    }
}