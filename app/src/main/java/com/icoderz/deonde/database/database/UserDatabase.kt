package com.icoderz.deonde.database.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.icoderz.deonde.database.dao.AddressDao
import com.icoderz.deonde.database.dao.UserDao
import com.icoderz.deonde.database.entity.SelectedAddressEntity
import com.icoderz.deonde.database.entity.ShippingAddressEntity
import com.icoderz.deonde.database.entity.UserEntity

@Database(
    entities = [UserEntity::class, ShippingAddressEntity::class, SelectedAddressEntity::class],
    version = 1,
    exportSchema = false
)
abstract class UserDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun addressDao(): AddressDao

    companion object {

        @Volatile
        private var INSTANCE: UserDatabase? = null

        fun getDatabase(context: Context): UserDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UserDatabase::class.java,
                    "user_database"
                ).build()

                INSTANCE = instance
                return INSTANCE as UserDatabase
            }
        }
    }
}