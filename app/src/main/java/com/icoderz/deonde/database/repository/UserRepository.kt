package com.icoderz.deonde.database.repository

import androidx.lifecycle.LiveData
import com.icoderz.deonde.database.dao.UserDao
import com.icoderz.deonde.database.entity.UserEntity

class UserRepository(private val userDao: UserDao) {

    val readAllData: LiveData<List<UserEntity>> = userDao.readAllData()

    suspend fun addUser(userEntity: UserEntity) {
        userDao.addUser(userEntity)
    }
}