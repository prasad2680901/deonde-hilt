package com.icoderz.deonde.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.icoderz.deonde.database.entity.SelectedAddressEntity
import com.icoderz.deonde.database.entity.ShippingAddressEntity

@Dao
interface AddressDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addAddress(shippingAddressEntity: ShippingAddressEntity)

    @Query("SELECT latitude FROM shipping_address ORDER BY shiping_id DESC LIMIT 1")
    fun getLatitude() : String

    @Query("SELECT longitutde FROM shipping_address ORDER BY  shiping_id DESC LIMIT 1")
    fun getLongitude() : String

    @Query("SELECT * FROM shipping_address ORDER BY shiping_id ASC")
    fun getAllShippingAddress() : LiveData<List<ShippingAddressEntity>>

    @Query("SELECT * FROM shipping_address WHERE latitude = :latitude and longitutde = :longitutde")
    fun getSelectedAddress( latitude : String,  longitutde : String) : SelectedAddressEntity
}