package com.icoderz.deonde.database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.icoderz.deonde.database.database.UserDatabase
import com.icoderz.deonde.database.entity.SelectedAddressEntity
import com.icoderz.deonde.database.entity.ShippingAddressEntity
import com.icoderz.deonde.database.repository.AddressRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddressViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var repository: AddressRepository
    private lateinit var getAllShippingAddress: LiveData<List<ShippingAddressEntity>>
    private lateinit var getSelectedAddress: SelectedAddressEntity

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val addressDao = UserDatabase.getDatabase(application).addressDao()
            repository = AddressRepository(addressDao)
            getAllShippingAddress = repository.getAllShippingAddress
            getSelectedAddress = repository.getSelectedShippingAddress
        }
    }

    fun addAddress(addressResponse: ShippingAddressEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addAddress(addressResponse)
        }
    }


    class AddressViewModelFactory(
        private val application: Application,
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return AddressViewModel(application) as T
        }
    }

}