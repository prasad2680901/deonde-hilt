package com.icoderz.deonde.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shipping_address")
data class ShippingAddressEntity(

    @PrimaryKey
    @ColumnInfo("shiping_id")
    val shippingId: Int,

    @ColumnInfo("Shiping_address")
    val shippingAddress: String,

    @ColumnInfo("house_name")
    val houseName: String?,

    @ColumnInfo("landmark")
    val landmark: String?,

    @ColumnInfo("flat_no")
    val flatNo: String?,

    @ColumnInfo("zip")
    val zip: String?,

    @ColumnInfo("city")
    val city: String,

    @ColumnInfo("state")
    val state: String,

    @ColumnInfo("adress_clarification")
    val addressClarification: String,

    @ColumnInfo("is_primary_address")
    val isPrimaryAddress: String,

    @ColumnInfo("latitude")
    val latitude: String,

    @ColumnInfo("longitutde")
    val logitude: String,
)
