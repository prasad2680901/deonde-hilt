package com.icoderz.deonde.database.repository

import androidx.lifecycle.LiveData
import com.icoderz.deonde.database.dao.AddressDao
import com.icoderz.deonde.database.entity.ShippingAddressEntity

class AddressRepository(private val addressDao: AddressDao) {

    val getAllShippingAddress: LiveData<List<ShippingAddressEntity>> =
        addressDao.getAllShippingAddress()
    val getSelectedShippingAddress =
        addressDao.getSelectedAddress(addressDao.getLatitude(), addressDao.getLongitude())

    suspend fun addAddress(shippingAddressEntity: ShippingAddressEntity) {
        addressDao.addAddress(shippingAddressEntity)
    }
}