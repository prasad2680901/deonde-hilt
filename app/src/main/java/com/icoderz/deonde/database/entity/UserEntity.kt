package com.icoderz.deonde.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class UserEntity(

    @PrimaryKey @ColumnInfo(name = "user_id") val userId: Int?,


    @ColumnInfo(name = "referral_code") val referralCode: String,

    @ColumnInfo(name = "user_role") val userRole: String,

    @ColumnInfo(name = "mobile_number") val mobileNumber: String,

    @ColumnInfo(name = "country_code") val countryCode: String?,

    @ColumnInfo(name = "user_name") val userName: String,

    @ColumnInfo(name = "last_name") val lastName: String?,

    @ColumnInfo(name = "user_email") val userEmail: String,

    @ColumnInfo(name = "is_language") val language: String,

    @ColumnInfo(name = "user_profile") val userProfile: String,

    @ColumnInfo(name = "verify_alcohol_image") val verifyAlcoholImage: String,

    @ColumnInfo(name = "alcohol_status") val alcoholStatus: String,

    @ColumnInfo(name = "user_notification_sound") val userNotificationSound: String?,

    @ColumnInfo(name = "status") val status: String,

    @ColumnInfo(name = "is_cod_available") val isCodAvailable: Int,

    @ColumnInfo(name = "wallet_status") val walletStatus: String,

    @ColumnInfo(name = "is_test_user") val isTestUser: Int,

    @ColumnInfo(name = "devicetoken") val deviceToken: String,

    @ColumnInfo(name = "wallet_amount") val walletAmount: Int,

    @ColumnInfo(name = "customise") val customise: String?,

    @ColumnInfo(name = "wallet_recharge_limit") val walletRechargeLimit: String?,

    @ColumnInfo(name = "maximum_wallet_amount") val maximumWalletAmount: String?,

    @ColumnInfo(name = "token") val token: String,

    @ColumnInfo(name = "wallet") val wallet: String?
)